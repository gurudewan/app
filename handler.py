import json
import numpy as np
import calculator

def hello(event, context):

    nothing = np.zeros(3)

    result = calculator.add(40, 2)

    body = {
        "message": "The answer is " + str(result) + " and here's nothing: " + str(nothing),
        #"input": event
    }

    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }

    return response
